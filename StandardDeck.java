public class StandardDeck extends Deck{
    public StandardDeck(){
    //creates a standard 52-card deck with each combination of the rank and suit values
        String [] ranks = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"};
        String [] suits = {"Hearts", "Diamonds", "Spades", "Clubs"};
    }
    public StandardCard dealTopCard(){
    //this method hides the Deck.dealTopCard() method. It retrieves the card from the parent class 
    //by calling dealTopCard() on the parent class and then casts the Card object to a StandardCard
    //object for ease of use.
        StandardCard topCard = super.dealTopCard();
        return topCard;
    }
}
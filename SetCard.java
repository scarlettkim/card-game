public class SetCard extends Card {
   /* SetCard(): creates a new card for the Set Game given a number, shape, shade and color.
   getShape(), getNumber(), getShading(), getColor(): getters for the attribute of a card from the Set Game.
        toString(): returns the description text for a card from the Set Game*/       
   private String shape;
   private int number;
   private String shading;
   private String color;
   public SetCard(String shape, int number, String shading, String color){
      this.shape = shape;
      this.number = number;
      this.shading = shading;
      this.color = color;
   }  
   public String getShape(){

      return shape;
   }
   public int getNumber(){

      return number;
   }
   public String getShading(){

      return shading;
   }
   public String getColor(){

      return color;
   }   
   //taken from parent class Card
  // public String toString(){
  // }
}
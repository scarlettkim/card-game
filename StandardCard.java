public class StandardCard extends Card {
   private String rank;
   private String suit;
   
   public StandardCard(Stirng rank, String suit){
      this.rank = rank;
      this.suit = suit;
   }
   public String getRank(){
      return rank; 
   }
   public String getSuit(){
      return suit;
   }
   //taken from parent class Card
 //  public String toString(){
  // }
}
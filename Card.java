public class Card {
    private String description;
    
    public Card(String cardText){
        description = cardText;
    }
    public String getCardText(){

        return description;
    }
    public String toString(){

        return description;
    }

}